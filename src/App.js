// import Name from "./Name";
// import Fname from "./FatherName";
// import Mname from "./motherName";
// import NewComponent from "./NewComponent";
// import ClassTernary from "./component/ClassTernary";
// import LearnInlineCss from "./component/LearnInlineCss";
// import LearnNullish from "./component/LearnNullish";
// import LearnOnClick from "./component/LearnOnClick";
// import LearnOr from "./component/LearnOr";
// import LearnTernaryOperator from "./component/LearnTernaryOperator";
// import MapPractise from "./component/MapPractice";
// import Storetag from "./component/Storetag";
// import LearnToHandleData from "./component/LearnToHandleData";
// import LearnUseState1 from "./component/LearnUseState/LearnUseState1";
// import LearnUseState2 from "./component/LearnUseState/LearnUseState2";

// import ClassWorkBlog from "./component/LearnRouting/ClassWorkBlog";
import LearnForm6 from "./component/LearnForm/LearnForm6";

// import LearnForm5 from "./component/LearnForm/LearnForm5";
// import LearnForm6 from "./component/LearnForm/LearnForm6";
// import ReadContacts from "./component/LearnForm/ReadContacts";

// import LearnForm3 from "./component/LearnForm/LearnForm3";
// import LearnForm4 from "./component/LearnForm/LearnForm4";

// import LearnForm1 from "./component/ LearnForm/LearnForm1";
// import LearnForm2 from "./component/LearnForm/LearnForm2";

// import { useState } from "react";
// import CleanUpFunction from "./component/LearnUseEffect/CleanUpFunction";
// import CleanUpFunction2 from "./component/LearnUseEffect/CleanUpFunction2";
// import LearnInfiniteLoop from "./component/LearnUseEffect/LearnInfiniteLoop";
// import LearnUseEffect7 from "./component/LearnUseEffect/LearnUseEffect7";

// import LearnUseEffect6 from "./component/LearnUseEffect/LearnUseEffect6";

// import LearnUseEffect5 from "./component/LearnUseEffect/LearnUseEffect5";

// import LearnUseEffect1 from "./component/LearnUseEffect/LearnUseEffect1";
// import LearnUseEffect2 from "./component/LearnUseEffect/LearnUseEffect2";
// import LearnUseEffect3 from "./component/LearnUseEffect/LearnUseEffect3";
// import LearnUseEffect4 from "./component/LearnUseEffect/LearnUseEffect4";
// import ExtraMemory from "./component/LearnUseState/ExtraMemory";
// import LearnUseState11 from "./component/LearnUseState/LearnUseState11";

// import LearnUseEffect1 from "./component/LearnUseEffect/LearnUseEffect1";

// import LearnUseState10 from "./component/LearnUseState/LearnUseState10";

// import LearnUseState7 from "./component/LearnUseState/LearnUseState7";
// import LearnUseState8 from "./component/LearnUseState/LearnUseState8";
// import LearnUseState9 from "./component/LearnUseState/LearnUseState9";

// import LearnUseState4 from "./component/LearnUseState/LearnUseState4";
// import LearnUseState6 from "./component/LearnUseState/LearnUseState6";

// import LearnUseState3 from "./component/LearnUseState/LearnUseState3";
function App() {
  // let [show, setShow] = useState(true);
  return (
    <div>
      {/* <p>My name is sudan</p>
      <h1>This is Heading 1</h1>
      <a href="https://www.youtube.com/" target="_blank">
        youtube.com
      </a>
      <img src="./earth.avif" alt="favicon"></img> */}
      {/* <Name></Name>
      <Fname></Fname>
      <Mname></Mname> */}
      {/* <NewComponent name="sudan" country="nepal" age={22}></NewComponent>{" "} */}
      {/* passing props */}
      {/* <LearnOr></LearnOr>
      <LearnNullish></LearnNullish>
      <LearnInlineCss></LearnInlineCss>
      <LearnOnClick></LearnOnClick>
      <Storetag></Storetag>
      <MapPractise></MapPractise>
      <LearnTernaryOperator></LearnTernaryOperator>
      <ClassTernary></ClassTernary> */}
      {/* <LearnToHandleData></LearnToHandleData> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <LearnUseState3></LearnUseState3> */}
      {/* <LearnUseState4></LearnUseState4> */}
      {/* <LearnUseState6></LearnUseState6> */}
      {/* <LearnUseState7></LearnUseState7> */}
      {/* <LearnUseState8></LearnUseState8> */}
      {/* <LearnUseState9></LearnUseState9> */}
      {/* <LearnUseState10></LearnUseState10> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <LearnUseState11></LearnUseState11> */}
      {/* <ExtraMemory></ExtraMemory> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}
      {/* <LearnUseEffect3></LearnUseEffect3> */}
      {/* <LearnUseEffect4></LearnUseEffect4> */}
      {/* <LearnUseEffect5></LearnUseEffect5> */}
      {/* <LearnUseEffect6></LearnUseEffect6> */}
      {/* <LearnUseEffect7></LearnUseEffect7> */}
      {/* {show ? <CleanUpFunction></CleanUpFunction> : null} */}
      {/* <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button>
      <button
        onClick={() => {
          setShow(true);
        }}
      >
        show
      </button> */}
      {/* {show ? <CleanUpFunction2></CleanUpFunction2> : null}
      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button>
      <button
        onClick={() => {
          setShow(true);
        }}
      >
        show
      </button> */}
      {/* <LearnInfiniteLoop></LearnInfiniteLoop> */}
      {/* <LearnForm1></LearnForm1> */}
      {/* <LearnForm2></LearnForm2> */}
      {/* <LearnForm3></LearnForm3> */}
      {/* <LearnForm4></LearnForm4> */}
      {/* <LearnForm5></LearnForm5> */}
      <LearnForm6></LearnForm6>
      {/* <ReadContacts></ReadContacts> */}
      {/* <Routing1></Routing1> */}
      {/* <ClassWorkBlog></ClassWorkBlog> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <UseState1></UseState1> */}
      {/* <UseState2></UseState2> */}
      {/* <UseState3></UseState3> */}
      {/* <LearnUseState6></LearnUseState6> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}
      {/* <LearnUseEffect3></LearnUseEffect3> */}
      {/* <LearnUseEffect7></LearnUseEffect7> */}
    </div>
  );
}

export default App;
