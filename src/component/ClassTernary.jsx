import React from 'react'

const ClassTernary = () => {
    let num = 200
  return (
    <div>
        {
            num <= 39 ? <div>Fail</div>
            :num >39 && num<= 59? <div>Third Division</div>
            :num >60 && num<=79 ?<div>First Division</div>
            :num >79 && num<=100 ?<div>Distinction</div>
            :null
        }
    </div>
  )
}

export default ClassTernary