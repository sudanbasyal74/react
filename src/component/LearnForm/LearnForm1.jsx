import React from "react";

const LearnForm1 = () => {
  return (
    <div>
      <form>
        {/* general input type */}
        <input type="text" placeholder="Enter Full Name" />
        <br />
        <input type="email" placeholder="Enter email" />
        <br />
        <input type="password" placeholder="password" />
        <br />
        <input type="number" />
        <br />
        <select>
          <option>Nepal</option>
          <option>India</option>
          <option>China</option>
        </select>
        <br />
        {/* specific input type */}
        <input type="radio" />
        <label>option 1</label>
        <br />
        <input type="radio" />
        option 2
        <br />
        <input type="radio" />
        option 3
        <br />
        <input type="checkbox" /> Are you gay?
      </form>
    </div>
  );
};

export default LearnForm1;
