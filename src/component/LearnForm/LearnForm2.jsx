import React, { useState } from "react";

const LearnForm2 = () => {
  let [name, setName] = useState("");
  let [age, setAge] = useState();
  let [address, setAddress] = useState("");
  let [password, setPassword] = useState("");
  let [description, setDescription] = useState("");
  return (
    <div>
      <form>
        <div>
          <label htmlFor="a">Name:</label>
          <input
            id="a"
            type="text"
            placeholder="Enter Your Name"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </div>
        <div>
          <label htmlFor="age">Age:</label>
          <input
            id="age"
            type="number"
            placeholder="Enter Age"
            value={age}
            onChange={(e) => {
              setAge(e.target.value);
            }}
          />
        </div>
        <div>
          <label htmlFor="address">Address:</label>
          <input
            id="address"
            type="text"
            placeholder="Enter Address"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          />
        </div>
        <div>
          <label htmlFor="pass">Password:</label>
          <input
            type="password"
            id="pass"
            placeholder="Enter Password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
              //console.log(e.target.value);
            }}
          />
        </div>
        <div>
          <label htmlFor="Description:">Description</label>
          <br />
          <textarea
            id="Description"
            cols="30"
            rows="10"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          ></textarea>
        </div>
      </form>
    </div>
  );
};

export default LearnForm2;
