import React, { useState } from "react";

const LearnForm3 = () => {
  let [country, setCountry] = useState("");
  return (
    <div>
      <form>
        <label>Country:</label>
        <select
          value={country}
          onChange={(e) => {
            setCountry(e.target.value);
          }}
        >
          <option value="nepal">Nepal</option>
          <option value="india">India</option>
          <option value="pakistan">Pakistan</option>
          <option value="china">China</option>
        </select>
      </form>
    </div>
  );
};

export default LearnForm3;
//when state value i.e country is equal to option value then the value is selected.
