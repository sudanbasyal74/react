import React, { useState } from "react";

const LearnForm4 = () => {
  let [gender, setGender] = useState("");
  return (
    <div>
      <form>
        <label htmlFor="male">Male</label>
        <input
          type="radio"
          id="male"
          value="male"
          checked={gender === "male"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        />
        <br />
        <label htmlFor="female">Female</label>
        <input
          type="radio"
          id="female"
          value="female"
          checked={gender === "female"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        />
        <br />
        <label htmlFor="others">Others</label>
        <input
          type="radio"
          id="others"
          value="others"
          checked={gender === "others"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        />
      </form>
    </div>
  );
};

export default LearnForm4;
