import React, { useState } from "react";

const LearnForm5 = () => {
  let [isMarried, setIsMarried] = useState("false");
  return (
    <div>
      <form>
        <label htmlFor="isMarried">isMarried</label>
        <input
          type="checkbox"
          id="isMarried"
          checked={isMarried === true}
          onChange={(e) => {
            setIsMarried(e.target.checked);
          }}
        />
      </form>
    </div>
  );
};

export default LearnForm5;
