import React, { useState } from "react";
import axios from "axios";
const LearnForm6 = () => {
  let [fullName, setFullName] = useState("");
  let [address, setAddress] = useState("");
  let [phoneNumber, setPhoneNumber] = useState();
  let [email, setEmail] = useState("");
  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();//prevents refresh
          let data = {
            fullName: fullName,
            address: address,
            phoneNumber: phoneNumber,
            email: email,
          };
          axios({
            url: "http://localhost:8000/api/v1/contacts",
            method: "POST",
            data: data,
          });
        }}
      >
        <label htmlFor="name">Full Name: </label>
        <input
          type="text"
          id="name"
          placeholder="Enter Your Name"
          value={fullName}
          onChange={(e) => {
            setFullName(e.target.value);
          }}
        />
        <br />
        <label htmlFor="address">Address: </label>
        <input
          type="text"
          id="address"
          placeholder="Enter Your Address"
          value={address}
          onChange={(e) => {
            setAddress(e.target.value);
          }}
        />
        <br />
        <label htmlFor="number">Phone Number: </label>
        <input
          type="number"
          id="number"
          placeholder="Enter Your Phone Number"
          value={phoneNumber}
          onChange={(e) => {
            setPhoneNumber(e.target.value);
          }}
        />
        <br />
        <label htmlFor="email">Email: </label>
        <input
          type="email"
          id="email"
          placeholder="Enter Your Email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default LearnForm6;
