import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
const ReadContacts = () => {
  let navigate = useNavigate();
  let [contact, setContact] = useState([]);
  let getReadContacts = async () => {
    let response = await axios({
      url: "http://localhost:8000/api/v1/contacts",
      method: "GET",
    });
    setContact(response.data.data.results);
  };
  console.log(contact);
  useEffect(() => {
    getReadContacts();
  }, []);
  return (
    <div>
      {contact.map((value, i) => {
        return (
          <div
            key={i}
            style={{ border: "solid red 2px", marginBottom: "20px" }}
          >
            <p>Full Name: {value.fullName}</p>
            <p>Address: {value.address}</p>
            <p>Phone Number: {value.phoneNumber}</p>
            <p>Email: {value.email}</p>
            <p>Created At: {new Date(value.createdAt).toLocaleString()}</p>
            <button
              onClick={async () => {
                await axios({
                  url: `http://localhost:8000/api/v1/contacts/${value._id}`,
                  method: "DELETE",
                });
                getReadContacts();
                // invalidation is required once the data is deleted so that the ui refreshes after the data is deleted from the database simultaneously
              }}
            >
              Delete
            </button>
            <button
              onClick={() => {
                navigate(`/contact/${value._id}`);
              }}
            >
              View
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadContacts;
