import React from "react";

const LearnInlineCss = () => {
  return (
    <div>
      <div
        style={{
          backgroundColor: "red",
          color: "white",
          padding: 20,
          textAlign: "center",
          display: "inline-block",
          border: 5,
        }}
      >
        Hello
      </div>
    </div>
  );
};

export default LearnInlineCss;
