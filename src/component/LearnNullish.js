import React from "react";

const LearnNullish = () => {
  let a = null;
  let b = "";
  let c = null;
  let d = a ?? b ?? c; //goes to next term only if the current term is either null or undefined
  console.log(d);
  return <div>LearnNullish</div>;
};

export default LearnNullish;
