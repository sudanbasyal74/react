import React from "react";
import { NavLink, Outlet, Route, Routes } from "react-router-dom";

const ClassWorkBlog = () => {
  return (
    <div>
      <NavLink to="/product">Products</NavLink>
      <Routes>
        {/* <Route path="/blog" element={<div>blog</div>}></Route>
        <Route path="/blog/:id" element={<div>blog/:id</div>}></Route>
        <Route path="/blog/create" element={<div>blog/create</div>}></Route>
        <Route
          path="/blog/update/:id"
          element={<div>blog/update/:id</div>}
        ></Route> */}
      {/* a better approach is as below */}
      {/* <Route path="blog" element={<Outlet></Outlet>}>
        <Route index element={<div>Read All Blogs</div>}></Route>
        <Route path=":id" element={<div>Detail Page</div>}></Route>
        <Route path="create" element={<div>Create Blog</div>}></Route>
        <Route path="update" element={<div><Outlet></Outlet></div>}>
          <Route path=":id" element={<div>Update Page</div>}></Route>
        </Route>
      </Route> */}
      <Route path="product" element={<Outlet></Outlet>}>
        <Route index element={<div>Read All the Products</div>}></Route>
        <Route path=":id" element={<div>This is Details Page</div>}></Route>
        <Route path="create" element={<div>Create Page</div>}></Route>
        <Route path="update" element={<div><Outlet></Outlet></div>}>
          <Route path=":id" element={<div>Update Products</div>}></Route>
        </Route>
      </Route>
      </Routes>
    </div>
  );
};

export default ClassWorkBlog;
