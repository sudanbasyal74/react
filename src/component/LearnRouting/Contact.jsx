import React from "react";
import { useSearchParams } from "react-router-dom";

const Contact = () => {
  let [queryParameter] = useSearchParams();
  console.log(`name = ${queryParameter.get("name")}`);
  console.log(`age = ${queryParameter.get("age")}`);
  return <div>Contact</div>;
};

export default Contact;
