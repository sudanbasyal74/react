import React from "react";
import { NavLink } from "react-router-dom";

const MyNavlink = () => {
  return (
    <div>
      {" "}
      <NavLink to="/" style={{ marginRight: "30px" }}>
        Home
      </NavLink>
      <NavLink to="/about" style={{ marginRight: "30px" }}>
        About
      </NavLink>
      <NavLink to="/contact" style={{ marginRight: "30px" }}>
        Read All Contacts
      </NavLink>
      <NavLink to="/contact/create" style={{ marginRight: "30px" }}>
        Create Contact
      </NavLink>
    </div>
  );
};

export default MyNavlink;
