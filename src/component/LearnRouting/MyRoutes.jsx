import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import Home from "./Home";
import About from "./About";
// import Contact from "./Contact";
import LearnForm6 from "../LearnForm/LearnForm6";
// import ContactDetails from "./ContactDetails";
import UpdateContacts from "./UpdateContacts";
import ReadContactDetails from "./ReadContactDetails";
import ReadContacts from "../LearnForm/ReadContacts";
const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        {/* <Route path="/signup" element={<LearnForm6></LearnForm6>}></Route> */}
        <Route path="/about" element={<About></About>}></Route>
        {/* <Route path="/contact" element={<Contact></Contact>}></Route> */}
        {/* to access query parameter check into Contact component */}
        <Route path="/contact" element={<div>hello</div>}></Route>{" "}
        {/* the route which comes first executes always so in this case Contact component is called*/}
        {/* localhost:3000/any */}
        {/* <Route
          path="/contact/:a"
          element={
            <div>
              <ContactDetails></ContactDetails>
            </div>
          }
        ></Route> */}
        {/* <Route
          path="/contact/a"
          element={<div>This is Contact ##############################/a</div>}
        ></Route> */}
        {/* here as per previous observation previous route should had been execute but in routing specificity matters and the route which is more specific is executed i.e the later one executes */}
        {/* localhost:3000/any/any */}
        {/* <Route
          path="/contact/:a/:b"
          element={<div>This is Contact ................... /c</div>}
        ></Route>
        <Route
          path="/dumbo"
          element={
            <div>
              <h1>I LOVE YOUUUUUUUUUUUU 🥺</h1>
            </div>
          }
        ></Route> */}
        {/* <Route
          path="*"
          element={
            <div>
              <h1>404 error</h1>
            </div>
          }
        ></Route> */}
        <Route path="contact" element={<Outlet></Outlet>}>
          <Route index element={<ReadContacts></ReadContacts>}></Route>
          <Route
            path=":id"
            element={<ReadContactDetails></ReadContactDetails>}
          ></Route>
          <Route path="create" element={<LearnForm6></LearnForm6>}></Route>
          <Route path="update" element={<Outlet></Outlet>}>
            <Route
              path=":id"
              element={<UpdateContacts></UpdateContacts>}
            ></Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;
