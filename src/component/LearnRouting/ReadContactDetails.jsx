import axios from "axios";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

const ReadContactDetails = () => {
  let [details, setDetails] = useState({});
  let params = useParams();
  let id = params.id;
  let getDetails = async () => {
    let response = await axios({
      url: `http://localhost:8000/api/v1/contacts/${id}`,
      method: "GET",
    });
    setDetails(response.data.data);
  };
  useEffect(() => {
    getDetails();
  }, []);

  return (
    <div>
      <p>ID: {details._id}</p>
      <p>Full Name: {details.fullName}</p>
      <p>Address: {details.address}</p>
      <p>Phone Number: {details.phoneNumber}</p>
      <p>Email: {details.email}</p>
      <p>Created At: {new Date(details.createdAt).toLocaleString()}</p>
      <p>Update At: {new Date(details.updatedAt).toLocaleString()}</p>
    </div>
  );
};

export default ReadContactDetails;
