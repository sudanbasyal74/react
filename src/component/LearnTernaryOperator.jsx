import React from 'react'

const LearnTernaryOperator = () => {
    let age = 17
  return (
    <div>
       {age === 16? <div>His Age is 16 </div>//?is ternary operator
       :age === 17? <div>His Age is 17 </div>
       :age === 18? <div>His Age is 18 </div>
       :null}
    </div>
  )
}

export default LearnTernaryOperator