import React from 'react'

const LearnToHandleData = () => {
    // string = is shown
    // number = is shown
    // boolean = is not shown in browser
    // null = is not shown in browser
    // undefined = is not shown in browser
    // array = [] and , is removed
    // object = cannot be called from div
    // let a = 'sudan'
    // let a = 1
    // let a = false
    // let a = null
    // let a = [1,2,3] 
    let a = {name:"sudan",age: 22}
  return (
    <div>
        {a.name}
        </div>
  )
}

export default LearnToHandleData