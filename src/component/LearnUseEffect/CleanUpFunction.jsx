import React, { useEffect } from "react";

const CleanUpFunction = () => {
  console.log(`i am component`);
  useEffect(() => {
    console.log(`i am useEffect`);
    return () => {
      console.log(`i am cleanup function`);
    };
  }, []);
  return <div>CleanUpFunction</div>;
};

export default CleanUpFunction;
