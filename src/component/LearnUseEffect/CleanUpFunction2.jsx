import React, { useEffect } from "react";

const CleanUpFunction2 = () => {

  useEffect(() => {
    let interval1 = setInterval(() => {
      console.log(`i execute after 2 seconds`);
    }, 2000);
    return () => {
      clearInterval(interval1);
    };
  }, []);
  return <div>CleanUpFunction2</div>;
};

export default CleanUpFunction2;
