import React, { useState } from "react";

const LearnInfiniteLoop = () => {
  let [count, setCount] = useState(0);
  setCount(count + 1); //always put setCount inside button-event e.g(onClick) or use effect else page re renders infinitely
  return (
    <div>
      count is :{count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnInfiniteLoop;
