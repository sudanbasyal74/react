import React, { useEffect, useState } from 'react'

const LearnUseEffect1 = () => {
  let [count,setCount]=useState (0)
  console.log(`i am component`)
  useEffect(()=>{
    console.log(`i am useEffect`)
  },[])
  return (
    <div> 
       count: {count}<br></br>
    <button onClick={()=>{
        setCount(()=>{
            return count+1
        })
    }}>
        increment
    </button></div>
  )
}

export default LearnUseEffect1