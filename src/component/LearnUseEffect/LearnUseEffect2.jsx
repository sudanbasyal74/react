import React, { useEffect, useState } from 'react'

const LearnUseEffect2 = () => {
  let [count,setCount]=useState (0)
  console.log(`i am component`)
  useEffect(()=>{
    console.log(`i am useEffect`)
  },[count])
  //[count] is dependency and if it changes useEffect runs from next render
  return (
    <div> 
       count: {count}<br></br>
    <button onClick={()=>{
        setCount(()=>{
            return count+1
        })
    }}>
        increment
    </button></div>
  )
}
export default LearnUseEffect2
