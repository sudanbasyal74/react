import React, { useEffect, useState } from 'react'

const LearnUseEffect3 = () => {
    let [count,setCount]=useState (0)
    let[_count,downCount] = useState(100)
    console.log(`i am component`)
    useEffect(()=>{
      console.log(`i am useEffect`)
    },[count,_count])
    //[count] is dependency and if it changes useEffect runs from next render
    return (
      <div> 
         count: {count}<br></br>
      <button onClick={()=>{
          setCount(()=>{
              return count+1
          })
      }}>
          increment
      </button>
      <br></br>
      count1 is:{_count}
      <br></br>
      <button onClick={()=>{
        downCount(()=>{
            return(_count+1)
        })
      }}>Increment</button>
      </div>
  )
}

export default LearnUseEffect3