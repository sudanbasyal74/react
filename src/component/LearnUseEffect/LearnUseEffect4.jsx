import React, { useEffect } from 'react'

const LearnUseEffect4 = () => {
    useEffect(()=>{
        console.log(`i am use effect`)
    })
    console.log(`i am component`)
  return (
    <div>LearnUseEffect4</div>
  )
}

export default LearnUseEffect4