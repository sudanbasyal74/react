import React, { useEffect, useState } from "react";

const LearnUseEffect5 = () => {
  let [arr1, setAr1] = useState([1, 2]);
  //always use primitive dependency
  useEffect(() => {
    console.log(`i am useEffect`);
  }, [JSON.stringify(arr1)]);
  return (
    <div>
      array is {arr1}
      <br></br>
      <button
        onClick={() => {
          setAr1([1, 2]);
        }}
      >
        change arr1
      </button>
    </div>
  );
};

export default LearnUseEffect5;
