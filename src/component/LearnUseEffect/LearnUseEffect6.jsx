import React, { useEffect, useState } from "react";

const LearnUseEffect6 = () => {
  //useEffect with no dependency, component renders
  let [count, setCount] = useState(0);
  useEffect(() => {
    console.log(`i am useEffect`);
  });
  //useEffect with empty dependency, component renders for the first time
  //   useEffect(() => {
  //     console.log(`i am useEffect`);
  //   }, []);
  //useEffect with primitive dependency, component renders for every change in dependency
  //   useEffect(() => {
  //     console.log(`i am useEffect`);
  //   }, [count]);
  //useEffect with primitive dependency, component renders if andy of the dependency change
  //   useEffect(() => {
  //     console.log(`i am useEffect`);
  //   },[count1,count2]);
  //convert non primitive dependency, convert it to primitive using JSON.stringify()
  return (
    <div>
      count is {count}
      <br></br>
      <button
        onClick={() => {
          setCount(() => {
            return count + 1;
          });
        }}
      >
        click
      </button>
    </div>
  );
};

export default LearnUseEffect6;
