import React, { useEffect, useState } from "react";

const LearnUseEffect7 = () => {
  let [count, setCount] = useState(0);
  useEffect(() => {
    console.log(`i am useEffect`);
    console.log(`a`);
    return () => {
      console.log(`i am clean up function`);
    };
  }, [count]);
  return (
    <div>
      count is: {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Click
      </button>
    </div>
  );
};

export default LearnUseEffect7;
