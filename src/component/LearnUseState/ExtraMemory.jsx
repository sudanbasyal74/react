import React, { useState } from 'react'

const ExtraMemory = () => {
    let[count,setCount] = useState(3)
  return (
    <div>
        count is:{count}<br></br>
        <button onClick={()=>{
            // setCount(count+1)//3+1=4
            // setCount(count+1)//3+1=4
            // setCount(count+1)//3+1=4
            setCount((pre)=>{
                return pre+1
            })
            setCount((pre)=>{
                return pre+1
            })
            setCount((pre)=>{
                return pre+1
            })
        }}>increment</button>
    </div>
  )
}

export default ExtraMemory

// component memory
// 3
// extra memory
// 3,4,4,4
//page renders as the value of count differs from one of the previous value