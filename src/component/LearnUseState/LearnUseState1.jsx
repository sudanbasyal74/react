import React, { useState } from 'react'

const LearnUseState1 = () => {
    //define variable using useState
    let [name, setName]=useState("ABC")//alternative of let name = "sudan"
    let[surname, _setName]=useState("DEF")
    return (
    <div>
        {name}<br></br>
        
        <button onClick={()=>{
            setName("GHI")
        }}>Change Name</button><br></br>
        {surname}<br></br>
        <button onClick={()=>{
            _setName("JKL")
        }}>Change Surname</button>
    </div>
  )
}
export default LearnUseState1




