import React, { useState } from 'react'

const LearnUseState10 = () => {
    let [count, setCount] = useState(0)//has a distinct memory `count`
    console.log(`hello`)
  return (
    <div>
        count = {count}<br></br>
        <button onClick={()=>{
            setCount(count+1)//has no distinct memory
        }}>Increase Count</button>
    </div>
  )
}

export default LearnUseState10
//even though the values are same, the component still renders as in case of non-primitive variables page renders if memory changes
// 