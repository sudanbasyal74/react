import React, { useState } from 'react'

const LearnUseState11 = () => {
    let [count,setCount]=useState(0)
  return (
    <div>
        count: {count}<br></br>
        <button onClick={()=>{
            // setCount(count+1)
            // there is another way to increase count as
            setCount(()=>{
                return count+1
            })
        }}>
            increment
        </button>
    </div>
  )
}

export default LearnUseState11