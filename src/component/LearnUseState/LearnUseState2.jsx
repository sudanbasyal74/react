import React, { useState } from 'react'

const LearnUseState2 = () => {
    let [count, setCount] = useState(0)
  return (

    <div>
        {count}<br></br>
        <button onClick={()=>{
            setCount(count+1)
        }}>Increment</button>
        <br></br>
        <button onClick={()=>{
            setCount(count-1)
        }}>Decrement</button>
        <br></br>
        <button onClick={()=>{
            setCount(0)
        }}>Reset</button>
    </div>
  )
}

export default LearnUseState2