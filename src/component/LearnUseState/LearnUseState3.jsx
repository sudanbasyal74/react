import React, { useState } from 'react'

const LearnUseState3 = () => {
    let [show,setShow]= useState()
  return (
    <div>
        {show?<img src='./favicon.ico' alt='favicon'></img>:null}
        <br></br>
        <button onClick={()=>{
            setShow(true)
        }}>show</button>
        <br></br>
        <button onClick={()=>{
            setShow(false)
        }}>Hide</button>



    </div>
  )
}

export default LearnUseState3