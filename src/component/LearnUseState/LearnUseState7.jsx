import React, { useState } from 'react'

const LearnUseState7 = () => {
  let[count,setCount] = useState(0)//count is a state variable
    return (
        <div>
            count is {count}<br></br>
            <button onClick={()=>{
                setCount(count+1)//  renders the page(re-execution of the page one every click) whenever state variable changes
            }}>Increment Count</button>


        </div>
        
  )
}

export default LearnUseState7