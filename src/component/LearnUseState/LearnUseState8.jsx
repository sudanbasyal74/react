import React, { useState } from 'react'

const LearnUseState8 = () => {
    let [count,setCount] = useState(0)
  return (
    <div>
        count is : {count}<br></br>
        {console.log(`hello`)}
        <button onClick={()=>{
            setCount(0)
        }}>Increment</button>
    </div>
  )
}

export default LearnUseState8
// the component will not render as the value of state variable does not change