import React, { useState } from 'react'

const LearnUseState9 = () => {
    let [count1,setCount1] = useState(0)
    let [count2,setCount2] = useState(100)
    console.log('hi')
  return (
    <div>
        count1 = {count1}<br></br>
        count2 = {count2}<br></br>
    <button onClick={()=>{
        setCount1(count1+1)
    }}>Increase Count1</button><br></br>
    <button onClick={()=>{
        setCount2(count2+1)
    }}>Increase Count2</button><br></br>
    </div>
  )
}

export default LearnUseState9