import React from 'react'

const MapPractice = () => {
  let ar1=[`abc`,`def`,`ghi`]
  return (
    <div>
      {ar1.map((value,i)=>{
        return <div key={i}>O/p: {value}</div>
      })}
    </div>
  )
}

export default MapPractice