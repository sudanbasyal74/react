import React from "react";
//to perform javascript operation in any tag we must use {}
const NewComponent = (props) => {
  return (
    <div>
      <p>props</p>
      <p>My Name is {props.name}</p>
      <p>I live in {props.country}</p>
      <p>My age is {props.age}</p>
    </div>
  );
};
export default NewComponent;
