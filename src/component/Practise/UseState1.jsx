import React, { useState } from 'react'
//to change surname
const UseState1 = () => {
    let [surname,setSurname] = useState("Khadka")
  return (
    <div>
        {surname}
        <button onClick={()=>{
            setSurname("Basyal")
        }}>Change Surname</button>
    </div>
  )
}
export default UseState1