import React, { useState } from "react";
//to increase and decrease count
const UseState2 = () => {
  let [count, setCount] = useState(0);
  return (
    <div>
      {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount(count - 1);
        }}
      >
        Decrement
      </button>
    </div>
  );
};

export default UseState2;
