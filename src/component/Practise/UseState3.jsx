import React, { useState } from "react";

const UseState3 = () => {
  let [show, setShow] = useState();
  return (
    <div>
      {show ? <img src="./favicon.ico" alt="favicon"></img> : null}
      <br />
      <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show
      </button>
      <button
        onClick={() => {
          setShow(false);
        }}
      >
        hide
      </button>
    </div>
  );
};

export default UseState3;
